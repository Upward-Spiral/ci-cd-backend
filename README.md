# Action Item: Software Lifecycle - Backend

In this Action Item we will deploy the Backend REST API we built in the previous week. If you haven't already, we recommend you complete the material on `Fullstack Thinking` before this one. The knowledge here, builds on the previous material.

**Make sure you set aside at least 2 hours of focused, uninterrupted work and give your best.**

In order to deploy the backend application we will:

1. Get an account on `GitLab`
2. Get an account on `AWS`
3. Create a `GitLab` repository for the backend app
4. Create a virtual server in `EC2` and deploy manually
5. Automatize the deployment by creating a pipeline in `Gitlab`

-----
## App Preparation:

#### 1. Install dependencies:

```bash
npm install
```

#### 2. Add a `.env` file with valid data (see the API Action Item for this):
```bash
echo "DB_URL=YOUR_MONGO_URL\nAUTH_PASS=supersecret" >> .env 
```

**Note: You can reuse the `.env` file from the API exercise here.**
#### 3. Run in production mode and check all works well:

```bash
npm run dev
```

---

# Step by Step
## 1. Get an account on `GitLab`

If you do not have one already, check the [SUPPORT.md](SUPPORT.md) file in this repository.

---

## 2. Get an account on `AWS`

If you do not have one already, check the [SUPPORT.md](SUPPORT.md) file in this repository.

---

## 3. Create a `GitLab` repository for the backend app

Check the [SUPPORT.md](SUPPORT.md) file in this repository for step by step instructions.

---

## 4. Create a virtual server in `EC2`, install `node.js` and deploy manually

4.1 Create a `linux` virtual server(instance) on `EC2`:

[Click here for: Quick Video Guide on Creating and Instance](https://codewithdragos.wistia.com/medias/p64fv65l88)

----

4.2 Connect to the virtual server on `EC2` with `ssh`, install `node.js and upload the files``

You can follow the [official documentation here](https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-up-node-on-ec2-instance.html) or watch the video bellow.

[Click here for: Quick Video Guide on installing Node.js](https://codewithdragos.wistia.com/medias/0phy6lfld7)

---

4.3 Upload the code with `scp` and deploy manually

You can now use `scp` to upload your code to the virtual instance. Replace `AWS_KEY_PATH` with the local path top the `.pem` file on your computer and `INSTANCE_IP` with the public ip of your instance and run the commands bellow:

```bash
chmod 400 AWS_KEY_PATH
# copy files to the ec2 instance
scp -i AWS_KEY_PATH -r build ec2-user@INSTANCE_IP:/home/ec2-user/build
scp -i AWS_KEY_PATH package.json ec2-user@INSTANCE_IP:/home/ec2-user/build/package.json
```

If you get stuck  [here is a Quick Video Guide](https://codewithdragos.wistia.com/medias/uw8axsqw57)

---

4.4 Create a `.env` file in the `AWS` instance
Run the `nano` editor in the `terminal` of the instance, inside the build folder:

```bash
nano .env
```

And add inside the content of your local `.env` file.

4.5 Install `pm2` on the `instance` and run your app:

*Note: PM2 runs node.js as a background process, so the app is live, even when we close our `ssh` session. We will get deeper on how it works later on.*


```bash
npm install pm2 -g
# in the ./build folder on the virtual machine -- instance
pm2 start index.js -i max
```

4.6 Check the application is now live on the `EC2` instance:

Go to [http://YOUR_INSTANCE_PUBLIC_IP/api-docs/](http://INSTANCE_IP/api-docs/)

---

## 5. Automatize the deployment by creating a pipeline in `Gitlab`

We have been able to deploy manually, but is very time consuming and error prone.

Let's automate the deployment using `Gitlab`.

---

### 1. In `Gitlab` go to pipelines:

![Create Pipeline Step 1](examples/add_pipeline_step_one.png)

---

### 2. Click **Create New Pipeline** && remove the default code:

![Create Pipeline Step 2](examples/add_pipeline_step_two.png)

---

### 3. Add the following code instead:

```yaml
image: node:latest
stages: ["build", "test", "deploy"]

# WARNING
# This pipeline needs the following variables set up to work:
# INSTANCE_IP =  // the public IP of the AWS instance
# SECRET_KEY = // the secret key to connect to the AWS instance (.pem) file
# ENV_FILE = // the .env file for production

# the build job
build:
  stage: build
  script:
    - npm install
    - npm run lint
    - npm run build
  artifacts:
    paths:
      - build
    expire_in: 1 week

# the test job
test:
  stage: test
  variables:
    MONGOMS_VERSION: 4.2.8
    MONGOMS_DEBUG: 1
    MONGOMS_DOWNLOAD_URL: https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-ubuntu1804-4.2.8.tgz
  script:
    - npm install
    - npm run test
  artifacts:
    paths:
      - coverage
    expire_in: 1 week

deploy:
  stage: deploy
  only:
    - main
  before_script: # prepare the pipeline runner for deploy by installing ssh
    - "which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )"
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    - apt-get update -y
    - apt-get -y install rsync
  script:
    - chmod 400 $SECRET_KEY
    # clean up the ec32 instance
    - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'rm -rf /home/ec2-user/api'
    - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'mkdir /home/ec2-user/api'
    
    # copy files to the ec2 instance
    - scp -i $SECRET_KEY -r build ec2-user@$INSTANCE_IP:/home/ec2-user/api
    - scp -i $SECRET_KEY package.json ec2-user@$INSTANCE_IP:/home/ec2-user/api/package.json
    - scp -i $SECRET_KEY deploy.sh ec2-user@$INSTANCE_IP:/home/ec2-user/api/deploy.sh
    
    # copy .env to the ec2 instance
    - scp -i $SECRET_KEY $ENV_FILE ec2-user@$INSTANCE_IP:/home/ec2-user/api/build/.env

    # run the deploy script
    - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'cd /home/ec2-user/api && bash deploy.sh'
  needs:
    - job: build
      artifacts: true
  dependencies:
    - build
```

---

5.3 Add a `deploy.sh` script to be executed by Gitlab in the deployment job:

Create the `deploy.sh` file in the root folder of the repository:
```bash
touch deploy.sh
```

An add to it the following code:
```bash
# clean up old app
pm2 kill
npm remove pm2 -g

# installing dependencies
echo "Installing npm2"
npm install pm2 -g
echo "Running npm install"
npm install

# start app with pm2
echo "Running the app"
npm run start:production
```

5.3 Add the production and monitor script to `package.json`:

```diff
  "scripts": {
    "lint": "eslint --fix",
    "test": "jest --coverage",
    "build": "rm -rf build && tsc && npm run copy-files",
    "start": "node build/index.js",
+   "start:production": "pm2 start build/index.js --name 'api' -f",
+   "monit": "pm2 monit",
    "dev": "nodemon src/index.ts",
    "copy-files": "copyfiles -u 1 src/**/*.yaml build/"
  },
```


5.3 Add the missing variables the `ci-cd` file needs: INSTANCE_IP, SECRET_KEY and ENV_FILE

- [x] add `INSTANCE_IP` to the `Gitlab` variables
![Add INSTANCE_IP](examples/add_instance_ip_variable.png)

----

- [x] add `SECRET_KEY` to the `Gitlab` variables
![Add SECRET_KE](examples/add_secret_key_variable.png)

----

- [x] add `ENV_FILE` to the `Gitlab` variables
![Add ENV_FILE](examples/add_env_file_variable.png)

5.6 Push your code:
```bash
git push
```

5.7 If the pipeline does not run automatically, run the pipeline manually:
![Run the pipeline](examples/add_pipeline_step_sixteen.png)


5.7 When the pipeline finished you should be live:
![API is live](examples/api_in_the_cloud.png)


---

Congratulations!!! You can now run applications in the cloud and deploy automatically!

## Wrapping up

1. Make sure you push and commit your code
2. Add a new instance as `staging`
3. Add a new stage, called staging that deploys to the new `instance`

### Made with :orange_heart: in Berlin by @CodeWithDragos
